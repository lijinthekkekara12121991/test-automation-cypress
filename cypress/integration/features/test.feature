Feature: Test feature

Scenario: Feature testing
	Given I click search type "Hotels"
	And I type destination "Munich"
	And I type checkin date "26/01/2021"
	And I type checkout date "27/01/2021"
	And I give number of adults as 2
	And I give number of children as 1
	When I click on search
	Then I should see the results page 
