class SearchPage {
	search_type(value) {
		cy.get('[data-name="hotels"]').contains(value).click()
	}

	destination(value) {
    cy.get('.form-group').first().click()
		cy.get('.select2-input').eq(7).type(value).wait(2000).type("{enter}")  
	}

	checkin_date(value) {
		cy.get('#checkin').clear().type(value)
    cy.get('i[class="bx bx-calendar"]').eq(0).click()
	}

	checkout_date(value) {
		cy.get('#checkout').clear().type(value)
    cy.get('i[class="bx bx-calendar"]').eq(1).click()
	}

  adults(value) {
    var i
    for (i = 0; i < value - 2; i++) {
      cy.get('input[name="adults"]').parent().find('button').first().click()
    }
  }

  children(value) {
    var i
    for (i = 0; i < value; i++) {
      cy.get('input[name="children"]').parent().find('button').first().click()
    }
  }

  search() {
    cy.get('button[type="submit"]:first').click()
  }
}
export default SearchPage
