class ResultsPage {
	was_page_loaded() {
		return cy.url().should('contain', '/search/')
	}
}
export default ResultsPage
