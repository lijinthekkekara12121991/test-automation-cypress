import { Given, And, When, Then } from 'cypress-cucumber-preprocessor/steps'
import SearchPage from './pages/searchpage'
import ResultsPage from './pages/resultspage'

const sp = new SearchPage()
const rp = new ResultsPage()

Given('I click search type {string}', (value) => {
	sp.search_type(value);
})

And('I type destination {string}', (value) => {
  sp.destination(value)
})

And('I type checkin date {string}', (value) => {
  sp.checkin_date(value)
})

And('I type checkout date {string}', (value) => {
  sp.checkout_date(value)
})

And('I give number of adults as {int}', (value) => {
  sp.adults(value)
})

And('I give number of children as {int}', (value) => {
  sp.children(value)
})

When('I click on search', () => {
  sp.search()
})

Then('I should see the results page', () => {
  rp.was_page_loaded()
})
